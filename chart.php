<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns#">

<! -- INDEX CHECKER -->

  <noscript><title>Trending check result.. - Youtube Trending Checker</title> </noscript>
          
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
        <script src="../js/jquery.js"></script>
      
      
	<!-- jQuery 
	<script src="/js/jquery-latest.min.js?v3.3.1"></script -->

	
	<link class="theme metro-dark" rel="stylesheet" href="/tablesorter-master/css/theme.metro-dark.css">

	<!-- Tablesorter script: required -->
	<script src="/tablesorter-master/js/jquery.tablesorter.js"></script>
	<script src="/tablesorter-master/js/jquery.tablesorter.widgets.js"></script>
	<script src="/tablesorter-master/js/widgets/widget-scroller.js"></script>
	
	<script src="/tablesorter-master/js/widgets/widget-sort2Hash.js"></script>
			
	<style id="css">/* Fixed column scroll bar spacer styling */
.tablesorter-scroller-bar-spacer {
	background: #eee;
}
/* add border to right side (LTR pages) of fixed column */
.tablesorter-scroller-fixed:after {
	content: '';
	border-right: 1px solid #444;
	width: 1px;
	position: absolute;
	top: 0;
	bottom: 0;
	z-index: 2;
	/* set to zero for non-jquery ui themes; use "left" here for RTL pages */
	right: 0;
	/* match the margins set to the table to keep the border the same height as the table */
	margin: 10px 0 15px;
}

/* using-x-theme added by the demo code */
.using-jui-theme .tablesorter-scroller-fixed:after {
	/* use "left" here for RTL pages */
	right: 0;
}
.using-green-theme .tablesorter-scroller-fixed:after,
.using-black-ice-theme .tablesorter-scroller-fixed:after,
.using-dark-theme .tablesorter-scroller-fixed:after,
.using-dropbox-theme .tablesorter-scroller-fixed:after {
	/* match the margins set to the table to keep the border the same height as the table */
	margin: 0;
}

/* OPTIONAL CSS! */
#fixed-columns-table tbody td {
	/* force "Notes" column to not wrap, so we get a horizontal scrolling demo! */
	white-space: nowrap;
	/* Add min column width, or "Index" column filter gets too narrow to use */
	min-width: 60px;
}</style>

	
	<script id="js">$(function() {

	$('.tablesorter').tablesorter({
		theme: 'metro-dark',
		showProcessing: false,
		headerTemplate : '{content} {icon}',
		widgets: [ 'uitheme', 'zebra', 'sort2Hash','filter', 'scroller' ],
		widgetOptions : {
			scroller_height : 300,
			// scroll tbody to top after sorting
			scroller_upAfterSort: true,
			// pop table header into view while scrolling up the page
			scroller_jumpToHeader: true,
			// In tablesorter v2.19.0 the scroll bar width is auto-detected
			// add a value here to override the auto-detected setting
			scroller_barWidth : null
			// scroll_idPrefix was removed in v2.18.0
			// scroller_idPrefix : 's_'
		}
	});

	var startFixedColumns = 0;

	$('#fixed-columns-table').tablesorter({
		theme: 'metro-dark',
		showProcessing: false,
		headerTemplate : '{content} {icon}',
		widgets: [ 'uitheme', 'zebra', 'sort2Hash','filter', 'scroller' ],
		widgetOptions : {
			// scroll tbody to top after sorting
			scroller_upAfterSort: true,
			// pop table header into view while scrolling up the page
			scroller_jumpToHeader: true,

			scroller_height : 700,
			// set number of columns to fix
			scroller_fixedColumns : startFixedColumns,
			// add a fixed column overlay for styling
			scroller_addFixedOverlay : false,
			// add hover highlighting to the fixed column (disable if it causes slowing)
			scroller_rowHighlight : 'hover',

			// bar width is now calculated; set a value to override
			scroller_barWidth : null
		}
	});

	// use jQuery UI slider to change the fixed column size
	$( '#slider' ).slider({
		value : startFixedColumns,
		min   : 0,
		max   : 4,
		step  : 1,
		slide : function( event, ui ) {
			// page indicator
			$( '.fixed-columns' ).text( ui.value );
			// method to update the fixed column size
			$( '#fixed-columns-table').trigger( 'setFixedColumnSize', ui.value );
		}
	});

	// update column value display
	$( '.fixed-columns' ).text( startFixedColumns );

});</script>
<script>
$(function() {

	var $jth = $('#jth'), // scroller_jumpToHeader toggle button
		$uas = $('#uas'), // scroller_upAfterSort toggle button
		themes = 'default blue green grey ice black-ice dark dropbox metro-dark',
		i, o = '', t = themes.split(' ');
	for (i = 0; i < t.length; i++) {
		o += '<option value="' + t[i] + '">' + t[i] + '</option>';
	}

	$('#theme')
		.append(o)
		.val('metro-dark')
		.change(function() {
			var theme = $(this).val().toLowerCase();

			// show jui select only if jQuery UI selected
			$('#switcher').toggle(theme === 'jui');
			// add class so the black border fits the theme
			$('#main').attr( 'class', 'using-' + theme + '-theme' );

			// refresh uitheme widget class names
			$('#main .tablesorter').each(function() {
				if (this.config) {
					this.config.theme = theme === 'black-ice' ? 'blackice' : theme;
				}
			});
			$('table').trigger('applyWidgets');

			// make sure columns align
			$(window).trigger('resize');
		}).change();

	$('button').click(function() {
		var jth = $jth.text() === 'true',
			uas = $uas.text() === 'true';
		if (this.id === 'jthb') {
			jth = !jth;
		} else {
			uas = !uas;
		}
		$('.tablesorter-scroller-table table').each(function() {
			var c = this.config;
			if (c) {
				c.widgetOptions.scroller_jumpToHeader = jth;
				c.widgetOptions.scroller_upAfterSort = uas;
			}
		});
		$jth.html(jth + '');
		$uas.html(uas + '');
		return false;
	});

});

</script>

</head>

<body>
  
<script>
	
</script>

<?php
/**
 * using mysqli_connect for database connection
 */

$databaseHost = 'localhost';
$databaseName = 'ytrendcheck';
$databaseUsername = 'root';
$databasePassword = 'root';

$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName); 

/*
$vid = $_POST['vid'];
$vidstamp = $_POST['vidstamp'];
$title = $_POST['title'];
$year = $_POST['year'];
$month =  $_POST['month'];
$day = $_POST['day'];
$view = $_POST['view'];
$like = $_POST['like'];
$dislike = $_POST['dislike'];
$comment = $_POST['comment'];
*/

// Fetch all users data from database
$sql = mysqli_query($mysqli, "SELECT * FROM `kchart`");
//$sql = mysqli_query($mysqli, "SELECT * FROM `kchart` WHERE day='27' OR day='28' ");

echo '<table class=tabclass><table id=fixed-columns-table><a href="/game-chart/">INDEX</a><thead>
    <tr>
<th>ID</th>
<th>VID</th>
<th>VidStamp</th>
<th>Title</th>
<th>Year</th>
<th>Month</th>
<th>Day</th>
<th>View</th>
<th>Like</th>
<th>Dislike</th>
<th>Comment</th>

    </tr>
  </thead>
  <tbody>';

/*
$sql = "SELECT view FROM kchart WHERE vid=\'Moq0aOiTUOA\' UNION SELECT view FROM kchart WHERE vid=\'Moq0aOiTUOA\'";
*/

while($row = mysqli_fetch_array($sql)){
	
	
echo "<tr><td>" . $row['id'] . "</td>"; 
echo "<td>" . $row['vid'] . "</td>"; 
echo "<td>" . $row['vidstamp'] . "</td>"; 
echo "<td>" . $row['title'] . "</td>"; 
echo "<td>" . $row['year'] . "</td>"; 
echo "<td>" . $row['month'] . "</td>";
echo "<td>" . $row['day'] . "</td>"; 
echo "<td>" . $row['view'] . "</td>";
echo "<td>" . $row['likee'] . "</td>"; 
echo "<td>" . $row['dislike'] . "</td>"; 
echo "<td>" . $row['comment'] . "</td>"; 
//echo "<td>" . $row[''] . "</td></tr>";  

}

echo "</tbody>
</table>";

mysql_close();

//print_r($sql);

?>

</body>
</html>
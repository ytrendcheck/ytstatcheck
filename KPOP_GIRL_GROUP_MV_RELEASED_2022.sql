-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 26, 2022 at 08:04 PM
-- Server version: 8.0.19-0ubuntu5
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ytrendcheck`
--

-- --------------------------------------------------------

--
-- Table structure for table `KPOP_GIRL_GROUP_MV_RELEASED_2022`
--

CREATE TABLE `KPOP_GIRL_GROUP_MV_RELEASED_2022` (
  `id` int NOT NULL,
  `pid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `pname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `vid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `publishedYear` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `publishedMonth` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `publishedDay` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `vidstamp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `year` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `month` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `day` int NOT NULL,
  `view` int NOT NULL DEFAULT '0',
  `likee` int NOT NULL DEFAULT '0',
  `dislike` int NOT NULL,
  `comment` int NOT NULL,
  `updateData` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `KPOP_GIRL_GROUP_MV_RELEASED_2022`
--
ALTER TABLE `KPOP_GIRL_GROUP_MV_RELEASED_2022`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vidstamp` (`vidstamp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `KPOP_GIRL_GROUP_MV_RELEASED_2022`
--
ALTER TABLE `KPOP_GIRL_GROUP_MV_RELEASED_2022`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

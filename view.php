<?php
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL); 

/**
 * using mysqli_connect for database connection
 */

$databaseHost = 'localhost';
$databaseName = 'ytstatcheck';
$databaseUsername = 'root';
$databasePassword = 'root';

$mysqli = mysqli_connect($databaseHost, $databaseUsername, $databasePassword, $databaseName); 

$panel = isset($_GET['panel']) ? $_GET['panel'] : '';
$pid = isset($_GET['pid']) ? $_GET['pid'] : '';
$pname = isset($_GET['pname']) ? $_GET['pname'] : '';
$publishedYear = isset($_GET['publishedYear']) ? $_GET['publishedYear'] : '2020';
$publishedMonth = isset($_GET['publishedMonth']) ? $_GET['publishedMonth'] : '12';
$publishedDay = isset($_GET['publishedDay']) ? $_GET['publishedDay'] : '1';
$table = isset($_GET['table']) ? $_GET['table'] : 'KCHART_KPOP_GIRL_ROOKIE_2020_MV';
$month = isset($_GET['month']) ? $_GET['month'] : '12';
$fromday = isset($_GET['fromday']) ? $_GET['fromday'] : '27';
$today = isset($_GET['today']) ? $_GET['today'] : '29';

$dataopt = isset($_GET['dataopt']) ? $_GET['dataopt'] : 'growth';
	
//$dataopt  = $_GET['dataopt'];

// http://localhost:8080/ytstatcheck/view.php?table=kchart&dataopt=growth&viewopt%5B%5D=view&fromday=27&today=29

//print_r($dataopt);

$viewopt = isset($_GET['viewopt']) ? $_GET['viewopt'] : '[0] => view';
	
	/* echo"<pre>";
	print_r($viewopt);
	echo"</pre>";
	*/
	// [0] => view
   //  [1] => likee
    // [2] => dislike
    // [3] => comment
	
	
	
	//print_r($val);
//$viewopt = $_GET['viewopt'];
//print_r($viewopt);

//$table = $_GET['table'];
//$fromday = $_GET['fromday'];
//$today = $_GET['today'];

include_once('head.inc.php');
?>
	
<body>
  
<a href="index.html">Back to Home </a> 
<br><br>
      <form type="text" action="view.php" method="get" name="form1">
    	
        <div class="row">       
   
 <label for="table">Playlist :</label>
  
<select id="table" name="table">
	
	<?php
	/*
if($table=="KPOP_GROUP_MV_RELEASED_2020"){$KPOP_GROUP_MV_RELEASED_2020_selected="selected=\"selected\"";}else{$KPOP_GROUP_MV_RELEASED_2020_selected="";}
 
if($table=="bpchart"){$bpchart_selected="selected=\"selected\"";}else{$bpchart_selected="";} 

if($table=="KCHART_KPOP_GIRL_ROOKIE_2020_MV"){$KCHART_KPOP_GIRL_ROOKIE_2020_MV_selected="selected=\"selected\"";}else{$KCHART_KPOP_GIRL_ROOKIE_2020_MV_selected="";} 

if($table=="KPOP_GIRL_GROUP_MV_RELEASED_2020"){$KPOP_GIRL_GROUP_MV_RELEASED_2020_selected="selected=\"selected\"";}else{$KPOP_GIRL_GROUP_MV_RELEASED_2020_selected="";} 
*/
?>
	
<?php
function PlaylistSelector($playlistname){
	global $table;
	//echo $table;
	if($table== "$playlistname"){
$playlistname_selected="selected=\"selected\"";
}else{
$playlistname_selected="";
} 
   echo "<option value=\"$playlistname\" $playlistname_selected>$playlistname</option>";
};

function DataOptSelector($dataopts,$label){
	global $dataopt;
	//echo $table;
	if($dataopt== "$dataopts"){
$dataopt_checked="checked=\"checked\"";
}else{
$dataopt_checked="";
} 
   echo "<input type=\"radio\" id=\"$dataopts\" name=\"dataopt\" value=\"$dataopts\" $dataopt_checked>
<label for=\"$dataopts\">$label</label><br>";
};
?>
	
<?php 
PlaylistSelector('KPOP_GIRL_GROUP_MV_RELEASED_2022');
PlaylistSelector('KPOP_GIRL_GROUP_MV_RELEASED_2020'); 

PlaylistSelector('KPOP_GROUP_MV_RELEASED_2020');
PlaylistSelector('KPOP_MV_2020_MOST_POPULAR_Kpop_World'); 
PlaylistSelector('KCHART_KPOP_GIRL_ROOKIE_2020_MV');  
PlaylistSelector('bpchart'); 

?>
		
  </select>
  
  <br>  <br>
<?php 
DataOptSelector('total','Total'); 
DataOptSelector('growth','Growth'); 
DataOptSelector('both','Total+Growth'); 
?>

       <br>  <br>
       	
   	 
<label for="view">( be aware, selecting all data causing Unrespossive, Hang, or crashing your browser or device! )<br>Choose Data :</label>
   	
     <select name="viewopt[]" multiple="multiple" id="dataselect" multiple>
     	
  <option value="view" Selected>View</option>

  <option value="likee">Like</option>
  <!--option value="dislike">Dislike</option-->
  <option value="comment">Comment</option>
</select>
     
       <br>  <br>

<?php
if($panel=="advancedfilter"){
       echo'<div class="col-25">
        <label for="subject">publishedYear : </label>
      </div>      
      
      <div class="col-25">
        <textarea id="subject"  name="month" placeholder="1" style="height:20px"><?php echo$publishedYear;?></textarea>
       	
       <div class="col-25">
        <label for="subject">publishedMonth : </label>
      </div>      
      
      <div class="col-25">
        <textarea id="subject"  name="month" placeholder="1" style="height:20px"><?php echo$publishedMonth;?></textarea>
        
       <div class="col-25">
        <label for="subject">publishedDay : </label>
      </div>      
      
      <div class="col-25">
        <textarea id="subject"  name="month" placeholder="1" style="height:20px"><?php echo$publishedDay;?></textarea>';        
      
 }
 
?>

<?php

function GetMonthDayData($datatype){
	//global $datatype;
	global $table;
	global $mysqli;
	
	if ($datatype =="month"){
		$sqlcolum = "month";
		$sqlfield = "month";
		$dataname = "month";
		}
	if ($datatype =="fromday"){
		$sqlcolum = "day";
		$sqlfield = "day";
		$dataname = "fromday";
		}
if ($datatype =="today"){
	$sqlcolum = "day";
		$sqlfield = "day";
		$dataname = "fromday";
		}
		
//$sql = "SELECT month,day FROM `$table` WHERE 1";
$sql = "SELECT DISTINCT $sqlcolum
FROM $table  ;";
//print_r($sql);

echo'
 <label for="'.$datatype.'">'.$datatype.' :</label>
<select id="'.$datatype.'" name="'.$datatype.'">';

$sql_stat = mysqli_query($mysqli, "$sql");
while($row = mysqli_fetch_array($sql_stat)){
	
		
	$getdata = $row[''.$sqlfield.''];
	//$day = $row['day'];
	
	//echo"<b> $datatype : $data</b><br>";
	//echo"<b> Day : $day</b><br>";
	
	
		
	echo'
<option name="'.$dataname.'" value="'.$getdata.'">
<label for="'.$datatype.'">'.$getdata.'</label><br>';
        
	
	}
	
echo'</select>';


//print_r($sql_stat);
//print_r($sql);
}


GetMonthDayData('month');
GetMonthDayData('fromday');

GetMonthDayData('today');
?>     
      <!--div class="col-25">
        <label for="subject">Month : </label>
      </div>      
      
      <div class="col-25">
        <textarea id="subject"  name="month" placeholder="1" style="height:20px"><?php echo$month;?></textarea>

       
      <div class="col-25">
        <label for="subject">From Day : </label>
      </div>      
      
      <div class="col-25">
        <textarea id="subject"  name="fromday" placeholder="1" style="height:20px"><?php echo$fromday;?></textarea>
      
            <div class="col-25">
        <label for="subject">To Day: </label>
      </div>      
      
      <div class="col-25">
        <textarea id="subject"  name="today" placeholder="31" style="height:20px"><?php echo$today;?></textarea-->
        <input type="submit" value="CHECK!">
      </div>
      
    </div>

    <!--input type="text" name="vid" value="CM4CkVFmTds"-->
    <!--input type="hidden" name="mode" value="offline"-->
    	
    </form>
    
<script>
	
</script>

<?php

//$vid = $_GET['vid'];
//$vid = $_POST['vid'];
//$table = $_GET['table'];
//$pid = $_GET['pid'];
//$pname = $_GET['pname'];



/*
$view = $_GET['view'];
$like = $_GET['like'];
$dislike = $_GET['dislike'];
$comment = $_GET['comment'];
*/

$monthNum = $month;
 $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
 
 //echo $monthName; // Output: May

 //  id,vid,vidstamp,title,year,month,day,view,likee,dislike,comment

// $sql = mysqli_query($mysqli, "SELECT vid,day,title,view,likee,dislike,comment FROM `$table` where day = '$fromday'  Order by view DESC");

// STATISTIC

function GetStatisticData($datatype){
	//global $datatype;
	global $table;
	global $month;
	global $fromday;
	global $mysqli;
$sql = "SELECT SUM($datatype) FROM `$table` WHERE month = '$month' and day = '$fromday' ";
$sql_stat = mysqli_query($mysqli, "$sql");
while($row = mysqli_fetch_array($sql_stat)){
	echo"<b>Total $datatype : " . number_format($row['SUM('.$datatype.')']) . "</b><br>";
	}

//print_r($sql_stat);
//print_r($sql);
}

GetStatisticData('view');
GetStatisticData('likee');
//GetStatisticData('dislike');
GetStatisticData('comment');

$query = "SELECT updateData FROM `$table` where month = '$month' and day = '$fromday'  limit 1";

$sql = mysqli_query($mysqli, "$query");

//print_r($query);

while($row = mysqli_fetch_array($sql)){
	echo"<b>Update Data : </b>" . $row['updateData'];
	}
	
echo"
<style>
.growthinfo{
font-size:14px;
margin-top:15px;
margin-bottom:-9px;
color:red;
}
</style>
<div class=\"growthinfo\"><b>* ";
foreach ($viewopt as $a){
    echo ucfirst(str_replace('likee','like',$a));
      }
      
$tructmonth = (strlen($monthName) > 3) ? substr($monthName, 0, 3) . '' : $monthName;      

echo' Growth from '.$fromday.' '.$tructmonth.' to '.$today.' '.$tructmonth.' 2020.</b></div>';

echo'<table idv="tabel" class="tablesorter" id=fixed-columns-table><!--a href="/game-chart/">INDEX</a--><thead>
    <tr>
    	
<th width="1%" class="filter-false"><b>#P</b></th>

<!--th>ID</th-->
<!--th>VID</th-->
<!--th>VidStamp</th-->
<style>
input.disabled { display: none; }
</style>
<th class="filter-false"><b>Group/Title</b><br>IG @YTrendCheck</th>';

foreach ($viewopt as $a){
    //echo $a;
    
        if ($dataopt == 'total'){
            echo'<th class="filter-false">Total<br>'.ucfirst(str_replace('likee','like',$a)).'</th>';
      }
    
    if ($dataopt == 'growth'){
           echo"<th class=\"filter-false\"><b>* str_replace('likee','like',$a)<br>Growth</b></th>";
      }
      
     if ($dataopt == 'both'){
            echo'<th class="filter-false"><b>'.ucfirst(str_replace('likee','like',$a)).'<br>total</b></th>';
           echo'<th class="filter-false"><b>* '.ucfirst(str_replace('likee','like',$a)).'<br>growth</b></th>';
      }
}

echo'
    </tr>
  </thead>
  <tbody>';


//SANITIZE A TITLE FOR USE
function cleanTitle($title) {
// $title = preg_replace('/\\s+/', '-', $title);
//    $title = preg_match('/^[a-zA-Z0-9 .]+$/', $title);
	return $title;
}

 //echo $monthName; // Output: May

 //  id,vid,vidstamp,title,year,month,day,view,likee,dislike,comment

// $sql2 = mysqli_query($mysqli, "SELECT vid,day,title,view,likee,dislike,comment FROM `$table` where day = '$fromday'  Order by view DESC");

$query2 = "SELECT vid,day,title,view,likee,dislike,comment FROM `$table` where month = '$month' and day = '$fromday'  ORDER BY `view` DESC limit 500";

//print_r($query2);

$sql2 = mysqli_query($mysqli, "$query2");

$arr = array();
$arr2 = array();
$number = 1;
$p = 1;
while($row = mysqli_fetch_array($sql2)){
	
	$arr[] = $row; 
	
$vid = $row['vid']; 

$title = $row['title'];

//https://res.cloudinviewopt.com/drlhixyyd/image/fetch/w_250,h_250,c_fill,g_face,f_auto/

$title = preg_replace('#[^0-9a-z .,-/]#i', '', $title);
$tructtitle = (strlen($title) > 22) ? substr($title, 0, 22) . '' : $title;
echo "<tr><td><b>#$p</b></td><td><a href=\"https://i.ytimg.com/vi/".$row["vid"]."/maxresdefault.jpg\" target=\"_blank\"><!--img src=\"https://i.ytimg.com/vi/".$row["vid"]."/default.jpg\" width=\"35\" height=\"16\" --></a> <b>" . $tructtitle . "</b></td>"; 

$p++;

foreach ($viewopt as $a){
    //echo $a;
    $data = $a;
    
    $prevview = $row[''.$a.'']; 
    
        if ($dataopt == 'total'){
        	
//$view = number_format($row[''.$a.''] , 0, '', ',');
$view = $row[''.$a.''];    

//echo "<td><b>" . $view . "</b></td>";        	
 echo "<td><b>";
print_r("

<script>
	//document.write('+');
document.write(test(".$view."));
</script>

");
echo "</b></td>";
           
      }    
      
        if ($dataopt == 'growth'){
        
echo'
<td><b style="color:red;">';
print_r("

<script>
	document.write('+');
document.write(format(".getgrowth($mysqli,$data,$table,$vid,$month,$today,$prevview)."));
</script>

");
echo"<b/></td>"; 

      }    
      
      if ($dataopt == 'both'){
      	
//$view = number_format($row[''.$a.''] , 0, '', ',');
$view = $row[''.$a.''];      

//echo "<td><b>" . $view . "</b></td>";
echo "<td><b>";
print_r("

<script>
	//document.write('+');
document.write(format(".$view."));
</script>

");
echo "</b></td>";
echo'
<td><b style="color:red;">';
print_r("

<script>
	document.write('+');
document.write(format(".getgrowth($mysqli,$data,$table,$vid,$month,$today,$prevview)."));
</script>

");
echo"<b/></td>"; 

      }    

}

echo "</tr>";

}

echo "</tbody>
</table>";

echo"<pre>";
//print_r($arr);
echo"</pre>";


echo"<pre>";
//print_r($arr2);
echo"</pre>";

function getgrowth($mysqli,$data,$table,$vid,$month,$today,$prevview){
	
// $sql2 = mysqli_query($mysqli, "SELECT  $data FROM `$table` where vid = '$vid' and day ='$today' Order by view DESC");

$sql2 = mysqli_query($mysqli, "SELECT  $data FROM `$table` where vid = '$vid' and month ='$month' and day ='$today' ");

//print_r("SELECT  $data FROM `$table` where vid = '$vid' and day ='$today' Order by view DESC");

//arsort($fruits);

while($row2 = mysqli_fetch_array($sql2)){
	
$arr2[] = $row2; 

$lastview = $row2[''.$data.''];  
//print_r($lastview);
//print_r($prevview);
$viewgrowth = $lastview - $prevview; 

if ($viewgrowth < 0 ){
$viewgrowth = 0;
}

$number = $viewgrowth;

if ($number < 1000000) {
    // Anything less than a million
    $format = number_format($number);
} else if ($number < 1000000000) {
    // Anything less than a billion
    $format = number_format($number / 1000000, 2) . 'M';
} else {
    // At least a billion
    $format = number_format($number / 1000000000, 2) . 'B';
}

//echo $format;
//echo $row['title'].'<br>'; 

// $viewgrowth = number_format($viewgrowth, 0, '', ',');

return $viewgrowth;
//$viewgrowth = $format;
//return $format;

}

}

?>
	
</body>
</html>